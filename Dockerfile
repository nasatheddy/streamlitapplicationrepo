FROM python:3.7-slim

RUN apt-get update -y

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 8501

COPY . /app
 
ENTRYPOINT ["streamlit", "run"]

CMD ["st_app.py"]

