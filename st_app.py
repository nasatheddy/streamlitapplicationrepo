
from sklearn import datasets
import pandas as pd
import streamlit as st
import pickle
import seaborn as sns
import matplotlib.pyplot as plt

st.title("This is my model of machine learning")


#@st.cache(allow_output_mutation = True)
def load_training_data():
    X,y = datasets.load_iris(return_X_y=True,as_frame=True)
    

    data = X.copy()
    data["target"] = y.values
    plt.figure(figsize=(10,10))
    fig = sns.pairplot(data,hue="target",palette='bright')
    st.pyplot(fig)

    return X,y


def load_model():
    with open("./model.pkl","rb") as f:
        res = pickle.load(f)
    return res


def user_input(X):

    st.subheader("Veuillez choisir vos valeurs :")
    sepal_len = st.slider('sepal length (cm)',min(X['sepal length (cm)']),max(X['sepal length (cm)']))
    sepal_wid = st.slider('sepal width (cm)',min(X['sepal width (cm)']),max(X['sepal width (cm)']))
    petal_len = st.slider('petal length (cm)',min(X['petal length (cm)']),max(X['petal length (cm)']))
    petal_wid = st.slider('petal width (cm)',min(X['petal width (cm)']),max(X['petal width (cm)']))
   
    user_data = pd.DataFrame({'sepal length (cm)':sepal_len, 'sepal width (cm)':sepal_wid, 'petal length (cm)':petal_len,
       'petal width (cm)':petal_wid},index=[0])


    return user_data

def prepare_user_data(scaler,data):
    return scaler.transform(data)


st.header("Exploration des données : ")

X,y = load_training_data()

# Load model
results = load_model()
model = results[0]
sc = results[1]


#user input
user_df = user_input(X)
user_df_prepared = prepare_user_data(sc,user_df)

# Prediction

target_names = ['setosa', 'versicolor', 'virginica']
prediction = model.predict(user_df_prepared)[0]
pred_proba = pd.DataFrame(model.predict_proba(user_df_prepared),index=[0],columns=target_names)


st.subheader("Table à prédire :")
st.table(user_df)

st.write(f"Cette fleur est dans la classe : --------------------- {target_names[prediction]}")
st.table(pred_proba)